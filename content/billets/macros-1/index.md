---
title: Le syndrome de Stockholm - vous allez adorer les macros SAS - Partie 1
date: "2022-09-01T00:00:00Z"
omit_header_text: true
featured_image: 'cover.jpg'
summary: En créant des macros SAS vous allez pouvoir simplifier l'écriture des requêtes à la base ORACLE et transformer SQL en véritable langage de programmation.
---

En créant des macros SAS vous allez pouvoir simplifier l'écriture des requêtes à
la base ORACLE et transformer SQL en véritable langage de programmation.

## Qu'est-ce qu'une macro SAS ?

Une macro est un motif de substitution de texte. Dans le billet sur les
[performances]({{< ref "/billets/performance" >}}) nous avons déjà rencontré la
macro `%connectora` qui est remplacée à l'exécution par les paramètres de
connexion au serveur ORACLE.

Une macro peut également prendre des paramètres. Imaginons, que nous souhaitions
obtenir la liste des bénéficiaires de plusieurs départements. Nous pourrions
écrire la macro suivante :

```sql
%macro beneficiaires_dpt(num);

  proc sql;
  %connectora;
  EXECUTE(
  
      create table BEN_DPT_&num. as
      select * from IR_BEN_R
      where BEN_RES_DPT = &num.)
      
  BY ORACLE;
  quit;
%mend;
```

Pour appeler cette macro pour le département 75 il suffit d'ajouter à la suite du
fichier script dans lequel vous avez saisi la macro ci-dessus :

```sql
%beneficiaires_dpt(075);
```

À l'exécution, la ligne précédente sera alors remplacée par le corps de la macro
et chaque occurrence de `&num.` sera elle substituée par `075`. Soit :

```sql
proc sql;
%connectora;
EXECUTE(

    create table BEN_DPT_075 as
    select * from IR_BEN_R
    where BEN_RES_DPT = 075)
    
BY ORACLE;
quit;
```

Si maintenant vous souhaitez créer une table de bénéficiaires pour chaque
département de la région Hauts-de-France :
```
%beneficiaires_dpt(002);   /*   Aisne         */
%beneficiaires_dpt(059);   /*   Nord          */
%beneficiaires_dpt(060);   /*   Oise          */
%beneficiaires_dpt(062);   /*   Pas-de-Calais */
%beneficiaires_dpt(080);   /*   Somme         */
```
## Faire une boucle avec une macro

Supposons que vous souhaitiez comptabiliser le nombre de chirurgies bariatriques
en PMSI/MCO. Si vous voulez réaliser cette opération pour chacune des années
entre 2010 et 2020, voici ce que vous pourriez être amené à écrire :

#### Tout d'abord une macro capable de faire cela sur une année donnée

```sql
%macro nb_chir_bar(annee_2_chiffres);

  proc sql;
  %connectora;
  EXECUTE(

     create table NB_CHIR_BAR_&annee_2_chiffres. as
     select COUNT(*) AS N
     from T_MCO&annee_2_chiffres.A
     where CDC_ACT in ('HFCA001', 'HFCC003', 'HFFA001', 'HFFA011',
                       'HFFC004', 'HFFC018', 'HFMA009', 'HFMA010',
                       'HFMC006', 'HFMC007', 'HGCA009', 'HGCC027')

  ) BY ORACLE;
  quit;

%mend;
```

#### Puis une macro pour la boucle sur une période

```sql
%macro actes_sur_annees(deb, fin);
  %DO annee=&deb. %TO &fin.;
      %nb_chir_bar(&annee.)
  %END;
%mend;
```

#### Enfin on exécute tout ceci sur la période voulue :

```sql
%actes_sur_annees(10, 20)
```

Vous obtenez 11 tables dans votre répertoire ORAUSER :

![Résultat de la macro boucle](img/resultat-loop-macro.png)


## Comment réutiliser des macros dans vos scripts ?

Il faut d'abord les définir dans des fichiers puis exécuter ces fichiers lors de
l'ouverture de votre session.

### Où positionner vos fichiers de macros ?

Voici une organisation qui vous permettra de facilement exporter vos macros
vers votre machine locale, ce qui peut être utile pour les partager ou les
archiver :

Il suffit de conserver vos macros dans votre répertoire `sasdata1/download`. Le
contenu de ce répertoire peut être exporté depuis le portail.

Dans la fenêtre serveur du client SAS Guide, vous trouverez le répertoire
`download` :

![Répertoire macros](img/repertoire-macros.png)

Dans l'onglet `Statistiques` de la page web du portail, vous trouverez le
panneau suivant, qui vous permettra de télécharger vos macros vers la machine
locale :

![Export depuis le portail](img/export-portail.png)

### Comment charger vos macros au démarrage de SAS ?

En effet, il est nécessaire de charger les fichiers de macros au lancement de
SAS pour pouvoir les utiliser dans vos programmes.

Un script SAS se lance à l'ouverture de chaque session, il se nomme `perso.sas`
et se trouve ici :

![Chemin perso sas](img/chemin-perso-sas.png)

Voici un exemple de fichier `perso.sas` où j'ajoute 3 fichiers de macros :

```sas
%put environnement personnel;

%let Fichiers=%sysget(HOME)/sasdata;

/* Macros configurées par défaut sur la plateforme : */
%include "&Fichiers/download/_macros_env.sas"; 

/*** Appeler vos macros : ***/
%include "&Fichiers/download/_macros_oracle.sas";
%include "&Fichiers/download/_macros_pmsi.sas";
%include "&Fichiers/download/_macros_dcir.sas";
```

+ `_macros_oracle.sas` pour [lancer des requêtes oracle plus simplement]( {{< ref "/billets/macros-2" >}} )
+ `_macros_pmsi.sas` pour [supprimer les doublons dans le PMSI]( {{< ref "/billets/macros-3" >}} )
+ `_macros_dcir.sas` pour [créer des outils pour le DCIR]( {{< ref "/billets/macros-4" >}} )

Chacun de ces fichiers est détaillé dans les billets en lien. 

À noter : la macro `%sysget(HOME)` vous permet de retrouver le chemin de votre répertoire personnel sur le serveur SAS.
