---
title: Mon repertoire de guitare
featured_image: 'img/guitar.jpg'
omit_header_text: true
---

Voici la liste des pièces que j'entretiens.
Elles sont dans un ordre aléatoire pour des questions de travail.

+ Red Hot Chili Peppers - Under the bridge / BPM 70
+ Lauro - Vals Venezolano No. 3 / BPM 65
+ Satie - Gnossienne 1
+ Brouwer - Study 10 / BPM 55 !! reprendre partition pour ryhtme
+ Brouwer - Study 5
+ Barrios - Preludio en Do Minor
+ Manuel de FALLA - Chanson du Feu Follet / BPM 70
+ Satie - Gymnopedie 1
+ Jackson 5 - Ben
+ Lauro - Vals Venezolano No. 2
+ Brouwer - Study 6 / BPM 70
+ Lauro - Vals Venezolano No. 1 / BPM 75
+ Bach - Cello suite 1 - Prelude
+ Brouwer - Study 11 / BPM 75
+ Brouwer - Study 2
+ Mudarra - Pavane et Gaillarde d'Alexandre
+ Tarrega - Lagrima
+ Brouwer - Study 1 / BPM 100
+ Bach - Well-Tempered Keyboard - C Major Prelude / BPM 37
+ Bach - Cello suite 2 - Courante
+ ??? - Llorona / BPM 60
+ Bach - Goldberg vario. 1
+ Brouwer - Study 3
+ Bach - Cello suite 2 - Prelude
+ Brouwer - Study 8
+ Brouwer - Study 4 / BPM 60
+ Tarrega - Capricho Árabe
+ Koji Kondo - Super Mario 2 Overwold Theme / BPM 60
+ Brouwer - Study 7
+ Brouwer - Study 9
+ Guilini E. - Praludie 1 / BPM 70
+ Dowland - Fortune my Foe
+ Granados - Danse espagnole 2 / BPM 60

-----
## Work in progress

+ Bach - Aria
+ Albéniz - Granada

-----

## Future pieces

+ Scarborough Fair
https://www.youtube.com/watch?v=4Ccgk8PXz64
https://tabs.ultimate-guitar.com/tab/simon-garfunkel/scarborough-fair-tabs-20229
+ Mrs Robinson
 https://www.youtube.com/watch?v=2Wx43y5xsP0
+ L'orage

-----
