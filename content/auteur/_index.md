---
title: Auteur

cascade:
  featured_image: '/img/avatar.png'
---

À propos de l'auteur de ce blog...


+ Ce blog a été créé par Pierre Balaye
+ `pierre [arobase] balaye [point] fr`
+ Mon CV : [Télécharger en PDF](pdf/CV_Balaye.pdf)
